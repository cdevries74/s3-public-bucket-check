# Scans S3 buckets in AWS account
# As aws cli is configured on the local machine
# Identify bucket open to public
# 10/2/2019 CD

import json
import boto3
import logging
from botocore.exceptions import ClientError

s3 = boto3.resource('s3')

def audit_bucket_policy(bucket_name):
    """Audit buckets in the AWS account
    Identify buckets in Public state
    """
    # Retrieve the list of bucket objects
    s3 = boto3.client('s3')
    try:
        response = s3.get_bucket_policy_status(Bucket=bucket_name)
    except ClientError as e:
        # AllAccessDisabled error == bucket not found
        # logging.error(e)
        return None
    return response['PolicyStatus']['IsPublic']

def bucket_list():
    s3 = boto3.resource('s3')
    my_list = []

    for bucket in s3.buckets.all():
        output = audit_bucket_policy(bucket.name)
        my_list.append({"name": bucket.name , "IsPublic": output})

    return my_list

if __name__ == "__main__":
    avacados = bucket_list()
    for i in avacados:
        report = i['IsPublic']
        if report == False:
            print(i['name'] + " is not Public")
        elif report == None:
            print(i['name'] + " -- Error: Bucket has no policy")
            pass